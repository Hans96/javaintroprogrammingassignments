package section3_apis.part2_collections;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * YOUR CHALLENGE:
 * This class only contains a so-called public API. There is no underlying code that supports the API.
 * It is your task to implement this logic. Using the right collection type(s).
 */
public class StudentAdmin {
    ArrayList<Student> allStudents = new ArrayList<>();
    HashMap<String, Course> results = new HashMap<>();




    /**
     * Returns the students that are present in the database.
     * If the searchString is * (a wildcard), all students will be returned. Else,
     * a simple case insensitive substring match to both first name and family name will be performed.
     * @param searchString the substring string to look for
     * @return students
     */
    public List<Student> getStudents(String searchString) {
        List<Student> nameStudents = new ArrayList<>();
        if (searchString.equals("*")) {
            return Collections.unmodifiableList(allStudents);
        } else {
            Pattern pattern = Pattern.compile(searchString);

            for (Student student : allStudents) {
                Matcher matcherFirstName = pattern.matcher(student.getFirstName().toLowerCase());
                Matcher matcherLastName = pattern.matcher(student.getLastName().toLowerCase());
                if(matcherFirstName.find() || matcherLastName.find()) {
                    nameStudents.add(student);

                }

            }
            return nameStudents;

        }

    }

    /**
     * Returns the grade of a student for the given course
     * @param student the student
     * @param course the course
     * @return grade
     */
    public double getGrade(Student student, Course course) {
        System.out.println(results.get(course.getCourseId()).grades);
        return results.get(course.getCourseId()).grades.get(student.getStudentId());
    }

    /**
     * returns all grades for a student, as [key=CourseID]:[value=Grade] Map
     * @param student the student to fetch grades for
     * @return grades
     */
    public Map<String, Double> getGradesForStudent(Student student) {
        Map<String, Double> studentResult = new HashMap<>();
        for (Course course : results.values()) {
            if(course.grades.containsKey(student.getStudentId())) {
                studentResult.put(course.getCourseId(), course.grades.get(student.getStudentId()));
            }

        }
        return studentResult;
    }

    /**
     * Returns all grades for a course, as [key=Student]:[value=Grade] Map
     * @param course the course
     * @return grades
     */
    public Map<Student, Double> getGradesForCourse(Course course) {
        Map<Student, Double> studentGrades = new HashMap<>();
        for (Student student: allStudents) {
            studentGrades.put(student, results.get(course.getCourseId()).grades.get(student.getStudentId()));
        }
        return studentGrades;


    }
}
