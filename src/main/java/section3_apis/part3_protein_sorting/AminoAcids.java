package section3_apis.part3_protein_sorting;

import java.util.HashMap;

public class AminoAcids {
    public static final HashMap<Character, Double> aminoAcids = new HashMap<>();

    static {
        aminoAcids.put('I', 131.1736);
        aminoAcids.put('L', 131.1736);
        aminoAcids.put('K', 146.1882);
        aminoAcids.put('M', 149.2124);
        aminoAcids.put('F', 165.1900);
        aminoAcids.put('T', 119.1197);
        aminoAcids.put('W', 204.2262);
        aminoAcids.put('V', 117.1469);
        aminoAcids.put('R', 174.2017);
        aminoAcids.put('H', 155.1552);
        aminoAcids.put('A', 89.0935);
        aminoAcids.put('N', 132.1184);
        aminoAcids.put('D', 133.1032);
        aminoAcids.put('C', 121.1590);
        aminoAcids.put('E', 147.1299);
        aminoAcids.put('Q', 146.1451);
        aminoAcids.put('G', 75.0669);
        aminoAcids.put('P', 115.1310);
        aminoAcids.put('S', 105.0930);
        aminoAcids.put('Y', 181.1894);


    }
}
