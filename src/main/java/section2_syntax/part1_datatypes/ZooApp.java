package section2_syntax.part1_datatypes;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ZooApp {

    public static void main(String[] args) {
        ZooApp zooApp = new ZooApp();
        zooApp.processZooData(args);
        zooApp.printZooSummary();
    }

    /**
     * Processes the command line data.
     * @param args
     */
    void processZooData(String[] args) {
        for (String animal: args) {
            ZooSpecies.registerSpeciesFromString(animal);
        }

    }

    /**
     * Prints a summary of the zoo.
     */
    void printZooSummary() {
        final List<ZooSpecies> allSpecies = ZooSpecies.getAllSpecies();

        System.out.println("The zoo has" + allSpecies.size() + ".");
        System.out.println("These are the species counts:");

        for (ZooSpecies species : allSpecies) {
            System.out.println(species + Integer.toString(species.getIndividualCount()));
        }
    }
}


