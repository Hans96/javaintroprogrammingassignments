package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    /**
     * Create an instance of class point that is located at the same coordinates as the current object (this), but in the
     * diagonally opposing quadrant of coordinate space.
     * So, when the current point is at (4, 4), this method will return Point(-4, -4),
     * and when the current point is at (2, -5) it will return Point(-2, 5).
     * @return inverse Point
     */
    Point createInversePoint() {
        Point point = new Point();
        if (x > 0) {
            point.x = -x;
        } else {
            point.x = Math.abs(x);
        }
        if (y > 0) {
            point.y = -y;
        } else {
            point.y = Math.abs(y);
        }
        return point;
    }

    /**
     * This method returns the Euclidean distance of the current point (this) to the given point (otherPoint).
     * @param otherPoint
     * @return euclidean distance
     */
    double euclideanDistanceTo(Point otherPoint) {
        return Math.sqrt(Math.pow(x-otherPoint.x, 2) + Math.pow(y-otherPoint.y, 2));

    }
}
