package section1_intro.part1_language_basics;

public class Square {
    Point upperLeft;
    Point lowerRight;

    /**
     * This method returns the surface defined by the rectangle with the given Point objects at the upper left and
     * lower right corners.
     * The method assumes two corners have been set already (are not null).
     * @return surface
     */
    int getSurface(){
        int y = upperLeft.y - lowerRight.y;
        int x = lowerRight.x - upperLeft.x;
        return y*x;
    }
}
