package section4_oop.part2_palindrome_finder;

import java.util.ArrayList;
import java.util.HashMap;

public class Palindrome {
    private static String palindrome = "";
    private static int recursionDepth = 0;
    private static ArrayList<Integer> misMatchIndex = new ArrayList<>();
    private static HashMap<Character, Character> nucleotides = new HashMap<>();
    private static int size = 0;



    public static void main(String[] args) {
        nucleotides.put('A', 'T');
        nucleotides.put('G', 'C');
        nucleotides.put('T','A');
        nucleotides.put('C', 'G');
        String line = "GGATAGCCGGgTATCC";
        size = line.length();
        getPalindrome(line);
        buildString(line);
        System.out.println(line);
        System.out.println(insertMisMatch(buildString(line)));

    }

    public static  void getPalindrome(String line) {
        if (line.length() > 1 ) {
            recursionDepth += 1;
            char[] begin = line.substring(0,1).toCharArray();
            char[] end = line.substring(line.length()-1).toCharArray();
            System.out.println(begin[0]);
            if(nucleotides.get(begin[0]).equals(end[0]))   {
                palindrome += "good";
            } else {
                misMatchIndex.add(size - recursionDepth);
            }
            getPalindrome(line.substring(1, line.length()-1));
        }



    }

    private static  StringBuilder buildString(String line) {
        StringBuilder resultString = new StringBuilder();
        resultString.append(">");
        for (int i = 0; i < line.length(); i++) {
            if (i < line.length()/2 -1) {
                System.out.println(line.length()/2);
                resultString.append(">");
            } else {
                resultString.append("<");
            }
        }
        return resultString;
    }

    private static String insertMisMatch(StringBuilder stringBuilder) {
        for (int i = 0; i < misMatchIndex.size(); i++) {
            stringBuilder.replace(misMatchIndex.get(i), misMatchIndex.get(i) , "*");
        }
        return stringBuilder.toString();

    }




}
