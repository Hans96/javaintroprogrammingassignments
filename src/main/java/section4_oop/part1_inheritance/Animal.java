/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Animal {
    private final String name;
    private int age;
    private String movementType;
    private double speed;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }




    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        return name;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        return movementType;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double maxSpeed, int maxAge) {
        speed = maxSpeed * (0.5 + (0.5 * ((maxAge - age) / (double)maxAge)));

    }

    public int getAge() {
        return age;
    }
    
}
