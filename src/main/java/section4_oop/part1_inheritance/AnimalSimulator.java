/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class AnimalSimulator {
    public static void main(String[] args) {
        AnimalSimulator anSim = new AnimalSimulator();
        anSim.start(args);
    }

    
    private void start(String[] args) {
        String animalName = args[0];
        int animalAge = Integer.parseInt(args[1]);
        for (int i = 0; i < args.length; i+=2) {
            if(getSupportedAnimals().contains(args[i])) {
                if (args[i].toLowerCase().equals("horse")) {
                    Horse horse = new Horse(args[i], Integer.parseInt(args[i+1]));
                    horse.setSpeed(horse.getMaxSpeed(), horse.getMaxAge());
                } else if (args[i].toLowerCase().equals("mouse")) {
                    Mouse mouse = new Mouse(args[i], Integer.parseInt(args[i+1]));
                    mouse.setSpeed(mouse.getMaxSpeed(), mouse.getMaxAge());
                    System.out.println(mouse.getSpeed());
                } else if (args[i].toLowerCase().equals("tortoise")) {
                    Tortoise tortoise = new Tortoise(args[i], Integer.parseInt(args[i+1]));
                    tortoise.setSpeed(tortoise.getMaxSpeed(), tortoise.getMaxAge());
                } else {
                    Elephant elephant = new Elephant(args[i], Integer.parseInt(args[i+1]));
                    elephant.setSpeed(elephant.getMaxSpeed(), elephant.getMaxAge());
                    System.out.println(elephant.getSpeed());
                }
            }

        }



    }

    /**
     * returns all supported animals as List, alphabetically ordered
     * @return supportedAnimals the supported animals
     */
    public List<String> getSupportedAnimals() {
        ArrayList<String> legalAnimals = new ArrayList<>();
        legalAnimals.add("Mouse");
        legalAnimals.add("Horse");
        legalAnimals.add("Elephant");
        legalAnimals.add("Tortoise");
        Collections.sort(legalAnimals);
        return legalAnimals;
    }
}
