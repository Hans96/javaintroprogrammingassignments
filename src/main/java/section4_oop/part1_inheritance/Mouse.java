package section4_oop.part1_inheritance;

public class Mouse extends Animal {
    private double maxSpeed = 21;
    private int maxAge = 13;

    public Mouse(String name, int age) {
        super(name, age);
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public int getMaxAge() {
        return maxAge;
    }


    @Override
    public String getMovementType() {
        return "scurry";
    }
}
