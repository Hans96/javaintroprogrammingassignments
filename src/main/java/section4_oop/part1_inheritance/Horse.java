package section4_oop.part1_inheritance;

public class Horse extends Animal {
    private double maxSpeed = 88;
    private int maxAge = 62;


    public double getMaxSpeed() {
        return maxSpeed;
    }

    public int getMaxAge() {
        return maxAge;
    }

    public Horse(String name, int age) {
        super(name, age);

    }

    @Override
    public String getMovementType() {
        return "gallop";
    }


}
